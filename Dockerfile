FROM node:20.11-alpine

WORKDIR /mobile

COPY . .

CMD ["/bin/bash -l -c \"chmod +x start.sh && ./start.sh\""]