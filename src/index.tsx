import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import * as serviceWorkerRegistration from './serviceWorkerRegistration';
import PWAPrompt from 'react-ios-pwa-prompt'
import 'bootstrap/dist/css/bootstrap.min.css';
import DeviceContextProvider from "./context/DeviceContextProvider";


const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <DeviceContextProvider>
    <React.StrictMode>
      <App /><PWAPrompt promptOnVisit={1} copyTitle="Adicionar a tela inicial" copyBody="Este site tem funcionalidade de aplicativo. Adicione-o à tela inicial para usá-lo em tela cheia e off-line."/>
    </React.StrictMode>
  </DeviceContextProvider>
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://cra.link/PWA
serviceWorkerRegistration.register();

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
