import React, { useContext } from 'react';
import { useState } from 'react'
import './style.css'
import fogaoIcon from '../../assets/fogao-icon.png'
import { useNavigate } from 'react-router-dom';
import DeviceContext from '../../context/DeviceContext';

let device: any;

function Home() {
  const navigate = useNavigate();
  const deviceContext = useContext(DeviceContext);

  async function connectBluetooth() {
    try {

      if (!deviceContext) {
        throw new Error("DeviceContext not found");
      }

      const { setDevice } = deviceContext;

      // @ts-ignore
      device = await navigator.bluetooth.requestDevice(
        {
          filters: [{
            services: ["4fafc201-1fb5-459e-8fcc-c5c9c331914b"]
          }],
          optionalServices: ["4fafc201-1fb5-459e-8fcc-d5a9c331737b", "4fafc201-1fb5-459e-8fcc-d5f9d331228a", "4fafc201-1fb5-459e-8fcc-d5f9d331229b"]
        }
      );
      setDevice(device);
      navigate('/working');
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <div className="App">
      <header className="App-header">
        <div className="titleIcon">
          <div className="title">SmartCooker</div>
          <img src={fogaoIcon} alt="logo-fogao" style={{height : '200px'}}/>
        </div>
        <button className='button' id="ble" onClick={() => connectBluetooth()}>Conectar Bluetooth</button>
      </header>
    </div>
  )

}

export default Home;