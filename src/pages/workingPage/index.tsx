import React, { useContext, useEffect, useState } from "react";
import "./style.css";
import { useLocation, useNavigate } from "react-router-dom";
import DeviceContext from "../../context/DeviceContext";
import NavBar from "../../components/navBar";

function WorkingPage() {
  const deviceContext = useContext(DeviceContext);

  if (!deviceContext) {
    throw new Error("DeviceContext not found");
  }

  let [min, setMin] = useState<number>(0);
  let [seg, setSeg] = useState<number>(0);
  let [time, setTime] = useState<number>(0);
  let [pow, setPow] = useState<string>("0");
  let [newpow, setNewpow] = useState<string>("1");

  const navigate = useNavigate();

  const handleClick = async () => {
    console.log("Entrou");
    try {
      const { device } = deviceContext;
      let server: any, service: any;

      server = await device.gatt.connect();

      service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5f9d331229b');

      let characteristic = await service.getCharacteristic('beb5418e-36a1-4688-b7f5-ea07361b68a2');

      var encoder = new TextEncoder();
      characteristic.writeValue(encoder.encode(newpow));
    } catch (error) {
      if (error instanceof DOMException) {
        if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
          navigate('/');
        } else {
          console.error(error);
        }
      } else {
        console.error(error);
      }
    }
  }

  useEffect(() => {
    const { device } = deviceContext;
    if (!device) {
      navigate('/');
      return;
    }
  
    const fetchData = async () => {
      try {
        const server = await device.gatt.connect();
        const service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5a9c331737b');
        const characteristic = await service.getCharacteristic('beb5487e-36e1-4688-b7f5-ea07361b79c1');
        const thing = await characteristic.readValue();
        const decoder = new TextDecoder("utf-8");
        console.log(decoder.decode(thing));
        const aux = decoder.decode(thing).split("|");
        const newTime = parseInt(aux[0].substring(0, 2)) * 60 + parseInt(aux[0].substring(3));
        setTime(newTime);
        if(newTime){
          setPow(aux[1]);
        } 
        setMin(Math.floor(newTime / 60));
        setSeg(newTime % 60);
      } catch (error) {
        if (error instanceof DOMException) {
          if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
            navigate('/');
          } else {
            console.error(error);
          }
        } else {
          console.error(error);
        }
      }
    };
  
    // Initial fetch after a delay
    const initialFetchTimeout = setTimeout(fetchData, 0);
  
    // Fetch data every 5 seconds
    const fetchInterval = setInterval(fetchData, 2000);
  
    // Update time every second
    const countdownInterval = setInterval(() => {
      setTime(prevTime => {
        if(prevTime > 0){
          const newTime = prevTime - 1;
          setMin(Math.floor(newTime / 60));
          setSeg(newTime % 60);
          return newTime;
        } else {
          setPow("0");
          return time;
        }
      });
    }, 1000);
  
    return () => {
      clearTimeout(initialFetchTimeout);
      clearInterval(fetchInterval);
      clearInterval(countdownInterval);
    };
  }, [deviceContext]);  

  return (
    <div className="App">
      <header className="App-header">
        <NavBar></NavBar>
        <div className="timer-display" style={{ fontSize: '100px', textAlign: 'center', margin: '50px' }}>
         {min < 10 && '0'}{min}:{seg < 10 && '0'}{seg}
        </div>
        <div>{pow}</div>
        <input type="range" min="0" max="6" value={pow} readOnly style={{pointerEvents: "none"}} />
        <div className="form-group mb-5">
          <label htmlFor="dropdown" className="form-label">Potência (Watts)</label>
          <select
            id="dropdown"
            name="dropdown"
            className="form-control"
            onChange={(e) => setNewpow(e.target.value)}
          >
            <option value="" disabled>Selecione uma opção</option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
          <button className="button" onClick={handleClick}><strong>Mudar Potência</strong></button>
        </div>
      </header>
    </div>
  );
}

export default WorkingPage;
