import React, { useState, useEffect, useContext } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import NavBar from '../../../components/navBar';
import DeviceContext from '../../../context/DeviceContext';

function EditRoutine() {
  const location = useLocation();
  const navigate = useNavigate();
  const deviceContext = useContext(DeviceContext);

  if (!deviceContext) {
    throw new Error("DeviceContext not found");
  }

  // Ajustando para usar um único estado para a rotina
  const [routine, setRoutine] = useState({
    power: "1",
    time: "",
    name: ""
  });
  const [timecor, setTimecor] = useState(true);
  const [namecor, setNamecor] = useState(true);
  const [index, setIndex] = useState(0);

  // Carregar dados da rotina na montagem do componente
  useEffect(() => {
    const state = location.state as { routine: string[], index: number };
    if (state && state.routine) {
      setRoutine({
        power: state.routine[0],
        time: state.routine[1],
        name: state.routine[2]
      });
      setIndex(state.index)
    }
  }, [location.state]);

  const handleSave = async (event: React.FormEvent) => {
    event.preventDefault();

    // Validação de tempo
    if (!/^([0-5]?[0-9]):([0-5]?[0-9])$/.test(routine.time)) {
      setTimecor(false);
      return;
    }
    setTimecor(true);

    // Validação de nome
    if (routine.name.length < 5 || routine.name.length > 50) {
      setNamecor(false);
      return;
    }
    setNamecor(true);

    try {
      const { device } = deviceContext;
      let server = await device.gatt.connect();
      let service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5f9d331228a');
      let characteristic = await service.getCharacteristic('beb5418e-36a1-4688-b7f5-ea07361b68d1');
      var decoder = new TextDecoder("utf-8");
      var encoder = new TextEncoder();

      // Ler as rotinas existentes
      var existingData = await characteristic.readValue();
      var routines = decoder.decode(existingData).split('|').filter(item => item);

      routines[(index * 3)] = routine.power;
      routines[(index * 3) + 1] = routine.time;
      routines[(index * 3) + 2] = routine.name;
      // }

      // Salvar de volta as rotinas atualizadas
      await characteristic.writeValue(encoder.encode(routines.join('|') + "|"));
      navigate("/routines"); // Redirecionar para a lista de rotinas
    } catch (error) {
      if (error instanceof DOMException) {
        if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
          navigate('/');
        } else {
          console.error(error);
        }
      } else {
        console.error(error);
      }
    }
  }
  return (
    <div className="App">
      <NavBar />
      <div className="App-header">
        {!timecor && (
          <div className="alert alert-danger">Tempo é obrigatório e deve estar no formato MM:SS</div>
        )}
        {!namecor && (
          <div className="alert alert-danger">Nome é obrigatório e deve ter entre 5 e 50 caracteres</div>
        )}
        <p className="bd-lead">Editar Receita</p>
        <form onSubmit={handleSave}>
          <div className="form-group mb-4">
            <label htmlFor="name" className="form-label">Nome (Mín: 5 caracteres, Máx: 50)</label>
            <input
              type="text"
              id="name"
              name="name"
              className="form-control"
              value={routine.name}
              onChange={(e) => setRoutine({ ...routine, name: e.target.value })}
            />
          </div>
          <div className="form-group mb-4">
            <label htmlFor="time" className="form-label">Tempo de Funcionamento (MM:SS)</label>
            <input
              type="text"
              id="time"
              name="time"
              className="form-control"
              value={routine.time}
              onChange={(e) => setRoutine({ ...routine, time: e.target.value })}
            />
          </div>
          <div className="form-group mb-5">
            <label htmlFor="power" className="form-label">Potência (Watts)</label>
            <select
              id="power"
              name="power"
              className="form-control"
              value={routine.power}
              onChange={(e) => setRoutine({ ...routine, power: e.target.value })}
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
          </div>
          <button type="submit" className="button"><strong>Salvar</strong></button>
        </form>
      </div>
    </div>
  );
}

export default EditRoutine;
