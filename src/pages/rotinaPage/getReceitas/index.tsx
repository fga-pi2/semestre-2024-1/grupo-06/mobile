import React, { useContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import DeviceContext from "../../../context/DeviceContext";
import NavBar from "../../../components/navBar";
import "./style.css";
import trash from '../../../assets/trash-icon.svg'
import edit from '../../../assets/edit-icon.svg'
import play from '../../../assets/play-icon.svg'


function GetRotinas() {
  const navigate = useNavigate();
  const deviceContext = useContext(DeviceContext);

  if (!deviceContext) {
    throw new Error("DeviceContext not found");
  }

  const [receitas, setReceitas] = useState<string[][]>([]);

  function goToCreateRoutine(){
    navigate("/createRoutine")
  }

  const handleDelete = async (index: number) => {
    try {
      const { device } = deviceContext;
      let server: any, service: any;
      server = await device.gatt.connect();
      service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5f9d331228a');
      let characteristic = await service.getCharacteristic('beb5418e-36a1-4688-b7f5-ea07361b68d1');

      let aux = [...receitas]; ;
      aux.splice(index, 1);

      let final = "";
      for (let i = 0; i < aux.length; i++) {
        final += aux[i][0] + "|" + aux[i][1] + "|" + aux[i][2] + "|";
      }
      var encoder = new TextEncoder();
      characteristic.writeValue(encoder.encode(final));
      setReceitas(aux);
    } catch (error) {
      if (error instanceof DOMException) {
        if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
          navigate('/');
        } else {
          console.error(error);
        }
      } else {
        console.error(error);
      }
    }
  };

  const handleEditar = (item: string[], index: number) => {
    navigate("/editRoutine", { state: { routine: item, index: index } });
  };

  const handleExecute = async (item: string[]) => {
    try {
      const power= item[0];
      const time = item[1];
      let min = parseInt(time.substring(0, 2));
      let seg = parseInt(time.substring(3, 5));

      if (min < 0 || min >= 60 || seg < 0 || seg >= 60) {
        throw new Error;
      }

      const { device } = deviceContext;
      let server: any, service: any;
      server = await device.gatt.connect();

      service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-c5c9c331914b');

      let characteristic = await service.getCharacteristic('beb5483e-36e1-4688-b7f5-ea07361b26a8');

      var thing = await characteristic.readValue();
      var decoder = new TextDecoder("utf-8");


      var encoder = new TextEncoder();
      characteristic.writeValue(encoder.encode(power + '|' + time + "|1"));

      thing = await characteristic.readValue();
      decoder = new TextDecoder("utf-8");
      navigate('/working');
    } catch (error) {
      if (error instanceof DOMException) {
        if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
          navigate('/');
        } else {
          console.error(error);
        }
      } else {
        console.error(error);
      }
    }
  }


  useEffect(() => {
    const { device } = deviceContext;
    if (!device) {
      navigate('/');
    }
    setTimeout(async () => {
      try {
        let server: any, service: any;
        server = await device.gatt.connect();
        service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5f9d331228a');
        let characteristic = await service.getCharacteristic('beb5418e-36a1-4688-b7f5-ea07361b68d1');

        var thing = await characteristic.readValue();
        var decoder = new TextDecoder("utf-8");
        let aux = decoder.decode(thing).split("|");
        aux.pop();
        setReceitas(aux.reduce((acc, _, index, arr) => {
          if (index % 3 === 0) {
            acc.push(arr.slice(index, index + 3));
          }
          return acc;
        }, [] as string[][]));
      } catch (error) {
        if (error instanceof DOMException) {
          if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
            navigate('/');
          } else {
            console.error(error);  
          }
        } else {
          console.error(error);
        }
      }
    }, 0);
  }, [deviceContext]);

  return (
    <div className="App">
    <header className="App-header">
        <NavBar />
        <button className="button on-button" onClick={goToCreateRoutine}>
          <strong>Criar rotina</strong>
        </button>
        {receitas.length > 0 && (
          <div className="table-container">
            <table>
              <thead>
                <tr>
                  <th colSpan={6}>Receitas</th>
                </tr>
              </thead>
              <tbody>
                {receitas.map((item, index) => (
                  <tr key={index} className={index % 2 === 0 ? 'even' : 'odd'}>
                    <td>{item[2]}</td>
                    <td>
                      <button onClick={() => handleDelete(index)}>
                        <img src={trash} alt="trash-icon" />
                      </button>
                    </td>
                    <td>
                      <button onClick={() => handleEditar(item, index)}>
                        <img src={edit} alt="edit-icon" />
                      </button>
                    </td>
                    <td>
                      <button onClick={() => handleExecute(item)}>
                        <img src={play} alt="play-icon" />
                      </button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        )}
      </header>
    </div>
  );
}

export default GetRotinas;
