import React, { useContext, useEffect, useState } from "react";
import "./style.css";
import NavBar from "../../../components/navBar";
import { useNavigate } from "react-router-dom";
import DeviceContext from "../../../context/DeviceContext";

function CreateRotinaCozimento(){

  const navigate = useNavigate();
  const deviceContext = useContext(DeviceContext);

  if (!deviceContext) {
    throw new Error("DeviceContext not found");
  }

  let [time, setTime] = useState<string>("");
  let [timecor, setTimecor] = useState<boolean>(true);
  let [name, setName] = useState<string>("");
  let [namecor, setNamecor] = useState<boolean>(true);
  let [power, setPower] = useState<string>("1");
 
  const handleSubmit = async (event: React.FormEvent) => {
    event.preventDefault();
    try {
      if (time.length !== 5 || time[2] !== ':') {
        throw new Error;
      }
      let inds = [0, 1, 3, 4];
      inds.forEach(function (ind) {
        if(time[ind] < '0' || time[ind] > '9'){
          throw new Error;
        }
      }); 
      let min = parseInt(time.substring(0, 2));
      let seg = parseInt(time.substring(3, 5));

      if (min < 0 || min >= 60 || seg < 0 || seg >= 60) {
        throw new Error;
      }
    } catch (error) {
      setTimecor(false);
      return;
    }
    setTimecor(true);

    try {
      if (name.length < 5 || name.length > 50) {
        throw new Error;
      }
    } catch (error) {
      setNamecor(false);
      return;
    }
    setNamecor(true);

    try {
      const { device } = deviceContext;

      let server: any, service: any;
      console.log(device);
      server = await device.gatt.connect();

      service = await server.getPrimaryService('4fafc201-1fb5-459e-8fcc-d5f9d331228a');

      let characteristic = await service.getCharacteristic('beb5418e-36a1-4688-b7f5-ea07361b68d1');

      var thing = await characteristic.readValue();

      var decoder = new TextDecoder("utf-8");

      var encoder = new TextEncoder();
      characteristic.writeValue(encoder.encode(decoder.decode(thing) + power + '|' + time + "|" + name + "|"));
      navigate("/routines");
    } catch (error) {
      if (error instanceof DOMException) {
        if (error.message.includes("Connection attempt failed") || error.message.includes("Bluetooth Device is no longer in range")) {
          navigate('/');
        } else {
          console.error(error);
        }
      } else {
        console.error(error);
      }
    }
  }

  return (
    <div className="App">
      <NavBar/>      
      <div className="App-header">
      {!timecor && (
          <div className="alert alert-danger">Tempo é obrigatório e deve estar no formato MM:SS</div>
        )}
        {!namecor && (
          <div className="alert alert-danger">Nome é obrigatório e deve ter entre 5 e 50 caracteres</div>
        )}
        <p className="bd-lead">Crie uma nova receita</p>
        <form>
          <div className="form-group mb-4">
            <label htmlFor="text" className="form-label">Nome (Mín: 5 caracteres, Máx: 50)</label>
            <input
              type="text"
              id="time"
              name="time"
              className="form-control"
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className="form-group mb-4">
            <label htmlFor="time" className="form-label">Tempo de Funcionamento (MM:SS)</label>
            <input
              type="text"
              id="time"
              name="time"
              className="form-control"
              onChange={(e) => setTime(e.target.value)}
            />
          </div>
          <div className="form-group mb-5">
            <label htmlFor="dropdown" className="form-label">Potência (Watts)</label>
            <select
              id="dropdown"
              name="dropdown"
              className="form-control"
              onChange={(e) => setPower(e.target.value)}
            >
              <option value="" disabled>Selecione uma opção</option>
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
          </div>
          <button type="submit" className="button" onClick={handleSubmit}><strong>Criar</strong></button>
        </form>
      </div>
    </div>
  );
}

export default CreateRotinaCozimento;
