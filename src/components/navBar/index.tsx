import React from "react";
import "./style.css";
import bluetooth from '../../assets/bluetooth.svg'
import { useNavigate } from "react-router-dom";

function NavBar() {
  const navigate = useNavigate();

  const handleClick = (destiny : string) => {
    navigate(destiny);
  };

  return (
    <nav className="stove-navbar navbar fixed-top navbar-expand-lg">
      <div className="container-fluid">
        <span className="navbar-icon">
          <img src={bluetooth} alt="Bluetooth" />
        </span>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarToggleExternalContent">
          <ul className="navbar-nav ms-auto">
            <li className="nav-item">
              <button className="nav-link" onClick={() => handleClick('/working')}>Estado</button>
            </li>
            {/* <li className="nav-item">
              <button className="nav-link" onClick={() => handleClick('/history')}>Histórico</button>
            </li> */}
            <li className="nav-item">
              <button className="nav-link" onClick={() => handleClick('/state')}>Ligar</button>
            </li>
            <li className="nav-item">
              <button className="nav-link" onClick={() => handleClick('/routines')}>Rotinas</button>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  );
}

export default NavBar;
