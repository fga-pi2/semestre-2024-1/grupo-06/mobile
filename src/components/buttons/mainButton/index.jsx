import React from "react";
import './style.css'

function Button(props) {
    const buttonText = props.text;
    const buttonColor = props.color ? '#DD3939' : '#232327';

    return(
        <button className="button" style={{backgroundColor: buttonColor}} onClick={props.onClick}>
            <span>{buttonText}</span>
        </button>
    )
}

export default Button;