import React, { useState } from "react";

function OnOffButton() {
  const [isOn, setIsOn] = useState(false);

  const handleClick = () => {
    setIsOn(!isOn);
  };

  return (
    <button
      className="button"
      style={{ backgroundColor: isOn ? "red" : "green" }}
      onClick={handleClick}
    >
      {isOn ? "Desligar" : "Ligar"}
    </button>
  );
}

export default OnOffButton;
