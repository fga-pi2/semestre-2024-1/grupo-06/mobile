import React from "react";
import Home from "./pages/homePage/index";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import StatePage from "./pages/statePage";
import CreateRotinaCozimento from "./pages/rotinaPage/createReceita"; 
import WorkingPage from "./pages/workingPage";
import GetRotinas from "./pages/rotinaPage/getReceitas";
import EditRoutine from "./pages/rotinaPage/editRoutine";

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/state" element={<StatePage />} />
        <Route path="/working" element={<WorkingPage />} />

        <Route
          path="/createRoutine"
          element={<CreateRotinaCozimento />}
        />
        <Route
          path="/editRoutine"
          element={<EditRoutine />}
        />
        <Route
          path="/routines"
          element={<GetRotinas />}
        />
        {/* <Route
          path="/create-rotina-cozimento"
          element={<CreateRotinaCozimento />}
        />{" "} */}
        {/* Nova rota para Rotina de Cozimento */}
      </Routes>
    </Router>
  );
}

export default App;
