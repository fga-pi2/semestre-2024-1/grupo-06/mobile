import React from 'react'

export type TodoContextType = {
    device: any;
    setDevice: (device: any) => void;
};

const DeviceContext = React.createContext<TodoContextType|null>(null);
export default DeviceContext;