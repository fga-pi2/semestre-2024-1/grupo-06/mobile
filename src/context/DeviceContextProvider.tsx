import * as React from 'react';
import DeviceContext, { TodoContextType } from './DeviceContext';

const DeviceContextProvider: React.FC<{children: React.ReactNode}> = ({ children }) => {
    const [device, setDevice] = React.useState<any>(null);
    return (
        <DeviceContext.Provider
            value={{ device, setDevice }}>
            {children}
        </DeviceContext.Provider>
    )
}

export default DeviceContextProvider;