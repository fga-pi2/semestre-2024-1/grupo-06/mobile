declare module 'react-ios-pwa-prompt' {
    import { ComponentType } from 'react';

    const PWAPrompt: ComponentType<any>;

    export default PWAPrompt;
}
